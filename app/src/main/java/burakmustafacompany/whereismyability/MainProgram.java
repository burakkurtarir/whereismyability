package burakmustafacompany.whereismyability;

import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.service.autofill.FillRequest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainProgram extends AppCompatActivity {


    public int valuePuan=0;
    public int valueSoruNumarasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_program);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();//Firebase'e bağlanma işlemi
        final DatabaseReference puan = database.getReference("Puan"); //Firebase'de Puan isimli değişken oluşturma işlemi
        //puan.setValue(100);

        final DatabaseReference soruNumarasi = database.getReference("SoruSayisi"); //Firebase'de SoruSayisi isimli değişken oluşturma işlemi
        //soruNumarasi.setValue(10);


        //final String[] key = new String[1];

        DatabaseReference database1;          //Deneme amaçlı yapılan  işler başlagıç
        database1 = FirebaseDatabase.getInstance().getReference("Puanlar");
        database1.child("Puan").setValue(45);
        database1.child("Puan").setValue(90);
        database1.child("Puan").setValue(145);   //Deneme amaçlı yapılan işler bitiş

        final TextView txtSoru = (TextView)findViewById(R.id.txtSoru); //TextView tanımlama

        final ImageStore imgStore = new ImageStore(); //Resim sınıfından nesne yaratma
        final Operations operations = new Operations(); //Operations sınıfından nesne yaratma
        final ImageView resimler = (ImageView) findViewById(R.id.imgView); //ImageViewi tanımlama
        Button buttonq = (Button) findViewById(R.id.btnQTusu); //Butonların tanımı
        Button buttonw = (Button) findViewById(R.id.btnWTusu);
        Button buttone = (Button) findViewById(R.id.btnETusu);
        Button buttonr = (Button) findViewById(R.id.btnRTusu);

        final TextView textPuan = (TextView)findViewById(R.id.txtPuan); //TextViewi tanımlama

        textPuan.setText("Puan : ");
        final TextView textPuan2 = (TextView)findViewById(R.id.txtPuan3); //TextView tanımlama
        textPuan2.setText("");

        puan.addListenerForSingleValueEvent(new ValueEventListener() { //Firebase'den puan okuma işlemi
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getValue().toString();
                valuePuan = Integer.parseInt(key);
                operations.setPuanManual(valuePuan);
                textPuan2.setText(""+operations.getPuan());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        soruNumarasi.addListenerForSingleValueEvent(new ValueEventListener() { //Firebase'den soru sayısı okuma işlemi
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getValue().toString(); //Değer okundu
                valueSoruNumarasi=Integer.parseInt(key); //İnteger'a dönüştürüldü
                //txtdeneme.setText(""+valueSoruNumarasi);
                operations.soruNumarasi = valueSoruNumarasi; //Soru numarası olarak atandı
                resimler.setImageResource(imgStore.images[operations.getSoruNumarasi()]); //Uygulama açıldığında ekranda çıkacak ilk resim
                txtSoru.setText("Soru No : "+operations.soruNumarasi);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        View.OnClickListener listenerq = new View.OnClickListener(){
            public void onClick(View view){ //Buton olayları
                char cevap = 'q'; //Q Butonuna basılırsa cevap q olur
                boolean sonuc=false;
                if(cevap==imgStore.cevaplar[operations.soruNumarasi]) //Cevap doğruysa sonuc true olur
                    sonuc=true;
                else if(cevap!=imgStore.cevaplar[operations.soruNumarasi]) //Yanlışsa false olur
                    sonuc=false;

                operations.setPuan(sonuc); //Puan, sonuca göre arttırılı veya azaltılır(20 artar, 10 azalır)
                textPuan2.setText(""+operations.getPuan()); //Puanı ekrana yaz
                operations.arttirSoruNumarasi(); //Bir sonraki soruya geç
                if(operations.soruNumarasi==imgStore.images.length) //Son soruya geldiysek başa dön
                    operations.soruNumarasi=0;

                txtSoru.setText("Soru No : "+operations.soruNumarasi); //Soru numarasını ekrana yaz

                puan.setValue(operations.getPuan()); //Puanı Firebase'e yaz
                soruNumarasi.setValue(operations.soruNumarasi); //Soru numarasını Firebase'e yaz

                resimler.setImageResource(imgStore.images[operations.soruNumarasi]); //Bir sonraki resme geç
            }
        };
        buttonq.setOnClickListener(listenerq); //Butona basılma olayı

        View.OnClickListener listenerw = new View.OnClickListener(){
            public void onClick(View view){
                char cevap = 'w';
                boolean sonuc=false;
                if(cevap==imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=true;
                else if(cevap!=imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=false;

                operations.setPuan(sonuc);
                textPuan2.setText(""+operations.getPuan());
                operations.arttirSoruNumarasi();
                if(operations.soruNumarasi==imgStore.images.length)
                    operations.soruNumarasi=0;

                txtSoru.setText("Soru No : "+operations.soruNumarasi);

                puan.setValue(operations.getPuan());
                soruNumarasi.setValue(operations.soruNumarasi);

                resimler.setImageResource(imgStore.images[operations.soruNumarasi]);
            }
        };
        buttonw.setOnClickListener(listenerw);

        View.OnClickListener listenere = new View.OnClickListener(){
            public void onClick(View view){
                char cevap = 'e';
                boolean sonuc=false;
                if(cevap==imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=true;
                else if(cevap!=imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=false;

                operations.setPuan(sonuc);
                textPuan2.setText(""+operations.getPuan());
                operations.arttirSoruNumarasi();
                if(operations.soruNumarasi==imgStore.images.length)
                    operations.soruNumarasi=0;

                txtSoru.setText("Soru No : "+operations.soruNumarasi);

                puan.setValue(operations.getPuan());
                soruNumarasi.setValue(operations.soruNumarasi);

                resimler.setImageResource(imgStore.images[operations.soruNumarasi]);
            }
        };
        buttone.setOnClickListener(listenere);

        View.OnClickListener listenerr = new View.OnClickListener(){
            public void onClick(View view){
                char cevap = 'r';
                boolean sonuc=false;
                if(cevap==imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=true;
                else if(cevap!=imgStore.cevaplar[operations.soruNumarasi])
                    sonuc=false;

                operations.setPuan(sonuc);
                textPuan2.setText(""+operations.getPuan());
                operations.arttirSoruNumarasi();
                if(operations.soruNumarasi==imgStore.images.length)
                    operations.soruNumarasi=0;

                txtSoru.setText("Soru No : "+operations.soruNumarasi);

                puan.setValue(operations.getPuan());
                soruNumarasi.setValue(operations.soruNumarasi);

                resimler.setImageResource(imgStore.images[operations.soruNumarasi]);
            }
        };
        buttonr.setOnClickListener(listenerr);
    }
}
